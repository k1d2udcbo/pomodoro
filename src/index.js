function iniciarPomodoro(){
    let trabalho = document.getElementById("trabalho").value;
    let pausa = document.getElementById("pausa").value;
    let sessoes = document.getElementById("sessoes").value;

    localStorage.setItem("trabalho",trabalho);
    localStorage.setItem("pausa",pausa);
    localStorage.setItem("sessoes",sessoes);

    window.location.replace("./index2.html");
}

window.onload = function() {
    document.getElementById("trabalho").value = 25;
    document.getElementById("pausa").value = 5;
    document.getElementById("sessoes").value = 3;
};

function aumenta(id){
    let element = document.getElementById(id);
    element.value = Number.parseInt(element.value)+1;
}

function diminui(id){
    document.getElementById(id).value -= 1;
}

